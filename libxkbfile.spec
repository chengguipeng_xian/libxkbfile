Name:		libxkbfile
Version:	1.1.0
Release:        2
Summary:	X11 keyboard file manipulation library
License:	MIT
URL:		https://www.x.org
Source0:	https://www.x.org/releases/individual/lib/%{name}-%{version}.tar.bz2

BuildRequires:	gcc xorg-x11-proto-devel libX11-devel

%description
Libxkbfile is used by the X servers and utilities to parse the XKB
configuration data files.

%package	devel
Summary: 	Development package for X11
Requires: 	%{name} = %{version}-%{release}

%description 	devel
This package is the development files for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS="%{optflags} -fno-strict-aliasing"
%configure
%make_build

%install
%make_install
%delete_la_and_a

%ldconfig_scriptlets

%files
%defattr(-,root,root)
%license COPYING
%doc ChangeLog
%{_libdir}/%{name}.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/X11/extensions/*.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/%{name}.so

%changelog
* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.0-2
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:modify the location of COPYING

* Fri Sep 6 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update

* Wed Aug 14 2019 openEuler Buildteam <buildteam@openeuler.org> 1.0.9-12
- Pacage init
